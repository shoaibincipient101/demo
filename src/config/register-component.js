import dashboard from '../components/Dashboard.vue';
import ProfileCard from '../components/ProfileCard.vue';
import DefaultLayout from '../components/common/DefaultLayout.vue';

export const registerComponents = (Vue) => {
    Vue.component('Dashboard', dashboard);
    Vue.component('profile', ProfileCard);
    Vue.component('DefaultLayout', DefaultLayout);

}