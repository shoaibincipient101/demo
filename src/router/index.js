import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Registration from '../components/Registration.vue';
// import Dashboard from '../components/Dashboard.vue';
import Profile from '../components/ProfileCard.vue';
import DashboardMain  from '../components/DashboardMain.vue';
Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Login',
    component: Home
  },
  {
    name: 'DashboardMain',
    path: '/app',
    component: DashboardMain,
    meta: { requiresAuth: true },
    children: [
      {
        name: 'profile',
        path: '/profile',
        component: Profile,
        meta: { requiresAuth: true }
      },
    ],
  },
  {
    path: '/registration',
    name: 'Registration',
    component: Registration,
    meta: { requiresAuth: false }
  },

]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})
router.beforeEach(async (to, from, next) => {
  const requiresAuth = to.matched.some(record => record.meta.requiresAuth);
  const currentUser = localStorage.getItem('user');
  if (requiresAuth && !currentUser) {
    next('/');
  } else {
    next();
  }
});

export default router
